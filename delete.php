<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "tck";

$id = $_REQUEST['id'];

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "DELETE FROM `employees` WHERE id = $id";
  echo $sql;
  $stmt = $conn->prepare($sql);
  $stmt->execute();
   
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}

$conn = null;

header("Location: listing.php");
?>

