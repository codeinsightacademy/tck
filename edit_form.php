<html>
<head>
<title>DMA Project</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"crossorigin="anonymous"></script>
</head>
<body class="d-flex flex-column h-100 container">
<header>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<div class="container-fluid">
<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
<div class="navbar-nav">
<a class="nav-link" href="index.html">Listing</a>
<a class="nav-link active" aria-current="page" href="add.html">Add New</a>
</div>
</div>
</div>
</nav>
</header>
<h3>Edit Employee Record</h3>

<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "tck";

$id = $_REQUEST['id'];

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "SELECT * FROM employees WHERE id = $id";
  $stmt = $conn->prepare($sql);
  $stmt->execute();

  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  $emp = $stmt->fetch();
   
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}

$conn = null;
/*
echo "<pre>";
print_r($emp);
echo "</pre>";
*/
?>


<form action="edit_action.php">

		<input type="hidden" name="id"  value="<?php echo $emp['id']; ?>"/>
		<div class="mb-3">
			<label for="name" class="form-label">Name</label>
			<input type="text" class="form-control" name="name" value="<?php echo $emp['name']; ?>" autofocus>
		</div>

		<div class="mb-3">
			<label for="exampleInputPassword1" class="form-label">City</label>
			<input type="text" class="form-control" name="city" value="<?php echo $emp['city']; ?>">
		</div>

		<div class="mb-3">
			<label for="city" class="form-label">Salary</label>
			<input type="text" class="form-control" name="salary" value="<?php echo $emp['sal']; ?>">
		</div>

		<input  class="btn btn-primary" type="submit"/>
		<a href="listing.php" class="btn btn-secondary">Cancel</a>
</form>





</div>
</footer>
</body>
</html>