<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>




<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "tck";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "SELECT * FROM employees";
  $stmt = $conn->prepare($sql);
  $stmt->execute();

  $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
  $emps = $stmt->fetchAll();
   
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}

$conn = null;
/*
echo "<pre>";
print_r($emps);
echo "</pre>";
*/
?>

<body class="container">

<a href="add_form.php" class="btn btn-secondary">Add New Employee</a>


<table class="table table-hover">
  <tr>
    <th>Id</th>
    <th>Name</th>
    <th>City</th>
    <th>Salary</th>
    <th>Action</th>
  </tr>
  
<?php foreach($emps as $e) { ?>
		 <tr>
			<td><?php echo $e['id'] ?></td>
			<td><?php echo $e['name'] ?></td>
			<td><?php echo $e['city'] ?></td>
			<td><?php echo $e['sal'] ?></td>
			<td>
				<a class="btn btn-primary" href="edit_form.php?id=<?php echo $e['id'] ?>">Edit</a>
				
				<a class="btn btn-danger" href="delete.php?id=<?php echo $e['id'] ?>" onclick="return confirm('Are you sure you want to delete record of <?php echo $e['name'] ?>?')">Delete</a>
			</td>
		  </tr>
<?php } ?>

</table>

</body>